FROM citusdata/citus

ENV POSTGIS_MAJOR 3
ENV POSTGIS_VERSION 3.3.3+dfsg-1.pgdg22.04+1

RUN apt update && apt install -y postgresql-postgis
